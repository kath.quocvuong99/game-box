const initalState = {
  game: [],
}

const futuredgameReducer = (state = initalState, action) => {
  switch (action.type){
    case 'SET_FUTEREDGAME': {
      return {
        ...state,
        game: action.payload.data,
      }
    }
    default:
      return state
  }
}

export default futuredgameReducer;