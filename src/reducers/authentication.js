const initalState = {
  isAuthentication: false,
}
const authenticationReducer = (state = initalState, action) => {
  switch (action.type) {
    default:
      return state;
  }
}
export default authenticationReducer;