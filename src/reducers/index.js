import { combineReducers } from "redux"
import authenticationReducer from "./authentication"
import futuredgameReducer from "./futuredgame"
import loginReducer from "./login"
import mostpopularrightnowReducer from "./mostpopularrightnow"
import navReducer from "./navbar"
import snackbarReducer from "./snackbar"
import yourgamingReducer from "./yourgaming"

const rootReducer = combineReducers({
  authentication: authenticationReducer,
  mostpopularrightnow: mostpopularrightnowReducer,
  yourgaming: yourgamingReducer,
  futuredgame: futuredgameReducer,
  navbar: navReducer,
  login: loginReducer,
  snackbar: snackbarReducer,
})
export default rootReducer