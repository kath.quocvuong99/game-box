import { Box, useTheme } from '@mui/material'
import React from 'react'
import { Outlet } from 'react-router-dom'
import AuthenticationDialog from '../components/dialog/login'
import Footer from '../components/footer'
import Header from '../components/header'
import SnackbarCustom from '../components/Snackbar'

export default function DefaultLayout() {
  const theme = useTheme()
  
  return (
    <Box
      sx={{
        padding: '15px 7.686676427525622vw 30px 7.686676427525622vw'
      }}
    >
      <Header/>
      <Box
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: '23px',
          padding: '4.392386530014641vw',
        }}
      >
        <Outlet />
      </Box>
      <Footer />
      <AuthenticationDialog />
      <SnackbarCustom />
    </Box>
  )
}
