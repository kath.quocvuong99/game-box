import React from 'react'
import { Route, Routes } from 'react-router-dom'
import DefaultLayout from '../layouts/DefaultLayout'
import Browse from '../pages/browse'
import Details from '../pages/details'
import CreateGame from '../pages/game/create'
import HomePage from '../pages/home'
import LoginPage from '../pages/login'
import RegisterPage from '../pages/register'
import Streams from '../pages/streams'
import { path } from './path'

export default function Router() {
  return (
    <Routes>
      <Route path={path.home} element={<DefaultLayout />}>
        <Route path={path.home} element={<HomePage />}/>
        <Route path={path.browse} element={<Browse />}/>
        <Route path={path.details} element={<Details />}/>
        <Route path={path.streams} element={<Streams />}/>
        <Route path={path.login} element={<LoginPage />}/>
        <Route path={path.register} element={<RegisterPage />} />
        <Route path={path.game_create} element={<CreateGame />} />
      </Route>
    </Routes>
  )
}
