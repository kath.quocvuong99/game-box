import React, { Fragment } from 'react'
import BannerHome from '../../components/banner-home'
import MostPopularRightNow from '../../components/most-popular-right-now'
import YourGamingLibrary from '../../components/your-gaming-library'

export default function HomePage() {

  return (
    <Fragment>
      <BannerHome />
      <MostPopularRightNow />
      <YourGamingLibrary />
    </Fragment >
  )
}
