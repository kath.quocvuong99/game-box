import { Box, Grid, Typography, useTheme } from '@mui/material'
import React from 'react'
import { useSelector } from 'react-redux';
import FeaturedGame from '../../components/featered-game';
import Title from '../../components/title';
import TopRank from '../../components/top-rank';
import ButtonMore from '../../components/button';
import VisibilityIcon from '@mui/icons-material/Visibility';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
import CheckIcon from '@mui/icons-material/Check';

export default function Browse() {
  const theme = useTheme()
  const { yourlive, livestreams } = useSelector(state => state.mostpopularrightnow)

  return (
    <Box>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
        }}
      >
        <FeaturedGame />
        <TopRank />
      </Box>
      <Box>
        <Box
          sx={{
            margin: '60px 0px',
            textAlign: 'center',
          }}
        >
          <Title
            titleLeft='How To Start Your'
            titleRight='Live Stream'
          />
        </Box>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'space-between'
          }}
        >
          {
            yourlive.map((item) => {
              return (
                <Box
                  key={item.id}
                  sx={{
                    padding: '30px',
                    border: `1px solid ${theme.palette.primary.main}`,
                    borderRadius: '23px',
                    width: '25.5%',
                    '& img': {
                      maxWidth: '60px',
                      borderRadius: '50%',
                    }
                  }}
                >
                  <img src={item.image} alt={item.title} />
                  <Typography
                    sx={{
                      fontSize: '20px',
                      margin: '20px 0px 15px 0px',
                      fontWeight: '600',
                      color: theme.palette.primary.light,
                    }}
                  >
                    {item.title}
                  </Typography>
                  <Typography
                    sx={{
                      fontSize: '15px',
                      color: theme.palette.primary.main,
                      lineHeight: '30px',
                    }}
                  >
                    {item.content}
                  </Typography>
                </Box>
              )
            })
          }
        </Box>
        <Box sx={{ marginTop: '30px', textAlign: 'center' }}>
          <ButtonMore contentButton='Go To Profile' />
        </Box>
      </Box>
      <Box
        sx={{
          marginTop: '60px',
          borderRadius: '23px',
          backgroundColor: theme.palette.primary.darker,
          padding: '30px',
        }}
      >
        <Box
          sx={{
            marginButtom: '30px',
          }}
        >
          <Title
            titleLeft='How To Start Your'
            titleRight='Live Stream'
          />
        </Box>
        <Box
          sx={{
            margin: '30px 0px'
          }}
        >
          <Grid container spacing={4}>
            {
              livestreams.map((item) => {
                return (
                  <Grid item sm={3} key={item.id}>
                    <Box
                      sx={{
                        '& .MuiTypography-root': {
                          display: 'none'
                        },
                        position: 'relative',
                        '& img': {
                          borderRadius: '23px',
                          width: '100%'
                        },
                        ':hover': {
                          '& .MuiTypography-root': {
                            display: 'unset',
                          },
                          '& .MuiBox-root': {
                            visibility: 'unset',
                          }
                        }
                      }}
                    >
                      <img src={item.streamBackground} alt={item.streamName} />
                      <Typography
                        sx={{
                          position: 'absolute',
                          backgroundColor: theme.palette.primary.lighter,
                          padding: '5px 10px',
                          borderRadius: '23px',
                          color: theme.palette.primary.light,
                          fontSize: '14px',
                          right: '1.0980966325036603vw',
                          top: '15px',
                          cursor: 'pointer',
                        }}
                      >
                        {item.status}
                      </Typography>
                      <Box
                        sx={{
                          position: 'absolute',
                          backgroundColor: theme.palette.primary.lighter,
                          padding: '3px 10px',
                          borderRadius: '23px',
                          color: theme.palette.primary.light,
                          fontSize: '14px',
                          bottom: '15px',
                          left: '15px',
                          cursor: 'pointer',
                          display: 'flex',
                          alignItems: 'center',
                          visibility: 'hidden',
                          '& svg': {
                            color: theme.palette.primary.light,
                          }
                        }}
                      >
                        <VisibilityIcon />
                        &nbsp;{item.view}
                      </Box>
                      <Box
                        sx={{
                          position: 'absolute',
                          backgroundColor: theme.palette.primary.lighter,
                          padding: '3px 10px',
                          borderRadius: '23px',
                          color: theme.palette.primary.light,
                          fontSize: '14px',
                          bottom: '15px',
                          right: '15px',
                          cursor: 'pointer',
                          display: 'flex',
                          alignItems: 'center',
                          visibility: 'hidden',
                          '& svg': {
                            color: theme.palette.primary.light,
                          }
                        }}
                      >
                        <SportsEsportsIcon />
                        &nbsp;{item.game}
                      </Box>
                    </Box>
                    <Box
                      sx={{
                        display: 'flex',
                        marginTop: '16px',
                      }}
                    >
                      <Box
                        sx={{
                          marginRight: '15px',
                          '& img': {
                            borderRadius: '50%',
                          }
                        }}
                      >
                        <img src={item.imageProfile} alt={item.streamName} />
                      </Box>
                      <Box>
                        <Box
                          sx={{
                            display: 'flex',
                          }}
                        >
                          <Box
                            sx={{
                              backgroundColor: theme.palette.primary.lighter,
                              borderRadius: '50%',
                              padding: '1px 4px',
                              marginRight: '3px',
                              '& svg': {
                                color: theme.palette.primary.light,
                                width: '12px',
                                height: '12px',
                              }
                            }}
                          >
                            <CheckIcon />
                          </Box>
                          <Typography
                            sx={{
                              fontSize: '14px',
                              color: theme.palette.primary.lighter,
                            }}
                          >
                            {item.streamName}
                          </Typography>
                        </Box>
                        <Typography
                          sx={{
                            marginTop: '8px',
                            fontSize: '20px',
                            fontWeight: '700',
                            color: theme.palette.primary.light,
                          }}
                        >
                          {item.title}
                        </Typography>
                      </Box>
                    </Box>
                  </Grid>
                )
              })
            }
          </Grid>
        </Box>
        <Box textAlign="center" mb="-50px">
          <ButtonMore contentButton='Discover Popular' />
        </Box>
      </Box>
    </Box>
  )
}
