import { Box, Grid, IconButton, Rating, Typography, useTheme } from '@mui/material'
import React from 'react'
import { useState } from 'react'
import { useForm } from 'react-hook-form'
import PublicIcon from '@mui/icons-material/Public';
import VpnLockIcon from '@mui/icons-material/VpnLock';
import { useMemo } from 'react';
import Selection from '../../../components/selection';
import TextFieldCustom from '../../../components/textfield-custum';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import { useDispatch } from 'react-redux';
import { SNACKBAR_ACTIONS } from '../../../actions';
import ButtonMore from '../../../components/button';

export default function CreateGame() {
  const theme = useTheme()
  const dispatch = useDispatch();
  const methods = useForm({
    mode: 'onSubmit',
    defaultValues: {
      title: '',
      description: '',
      storage: '',
      download: 0,
      imageUrl: '',
      rate: '2.5',
    }
  });
  const {
    handleSubmit,
    control,
    setValue,
    reset,
  } = methods;
  const [imageLink, setImageLink] = useState('')
  const status = useMemo(() => {
    return [
      {
        name: 'Công khai',
        value: 'public',
        icon: <PublicIcon />,
        isDefault: true,
      },
      {
        name: 'Riêng tư',
        value: 'private',
        icon: <VpnLockIcon />,
      }
    ]
  }, [])
  const Submitgame = (data) => {
    console.log(data, '---data')
  }
  const handleOnBlur = (value) => {
    setImageLink(value)
  }
  const handleClearImg = () => {
    setImageLink('')
    setValue('imageUrl', '')
  }
  const handleOnClearInput = () => {
    reset();
    setImageLink('');
  }
  const error = () => {
    dispatch({ type: SNACKBAR_ACTIONS.OPEN, message: 'Vui lòng kiểm tra dữ liệu' });
  };

  return (
    <form onSubmit={handleSubmit(Submitgame,error)}>
      <Box>
        <Grid container justifyContent="space-between" alignItems='center' mb={3}>
          <Grid item sm={6}>
            <Typography
              fontSize="32px"
              color={theme.palette.primary.lighter}
            >
              Tạo mới Game
            </Typography>
          </Grid>
          <Grid
            item
            sm={2.5}
            sx={{
              '& .MuiInputBase-root': {
                borderRadius: '24px',
                border: `solid 1px ${theme.palette.primary.light}`
              }
            }}
            >
            <Selection
              name="isPublic"
              options={status}
              control={control}
              isRequired
            />
          </Grid>
        </Grid>
        <Box
          sx={{
            '& .MuiOutlinedInput-notchedOutline': {
              border: `1px solid ${theme.palette.primary.light}`,
            },
            '& input, textarea': {
              color: theme.palette.primary.light,
            }

          }}
        >
          <Box>
            <Typography
              fontSize="17px"
              color={theme.palette.primary.light}
              mb={0.5}
            >
              Tên game
            </Typography>
            <TextFieldCustom
              fullWidth
              name="title"
              placeholder="Nhập tên game"
              control={control}
              isRequired
            />
          </Box>
          <Box mt={3}>
            <Typography
              fontSize="17px"
              color={theme.palette.primary.light}
              mb={0.5}
            >
              Mô tả game
            </Typography>
            <TextFieldCustom
              fullWidth
              name="description"
              placeholder="Nhập mô tả game"
              type="textarea"
              rows={5}
              control={control}
              isRequired
            />
          </Box>
          <Box mt={3}>
            <Grid container justifyContent="space-between">
              <Grid item sm={3}>
                <Typography
                  fontSize="17px"
                  color={theme.palette.primary.light}
                  mb={0.5}
                >
                  Số lượng downloaded
                </Typography>
                <TextFieldCustom
                  fullWidth
                  name="download"
                  disabled
                  control={control}
                  defaultValue={0}
                />
              </Grid>
              <Grid item sm={3}>
                <Typography
                  fontSize="17px"
                  color={theme.palette.primary.light}
                  mb={0.5}
                >
                  Dung lượng game
                </Typography>
                <TextFieldCustom
                  fullWidth
                  name="storage"
                  placeholder="Nhập dung lượng"
                  control={control}
                  isRequired
                />
              </Grid>
              <Grid item sm={3}>
                <Typography
                  fontSize="17px"
                  color={theme.palette.primary.light}
                  mb={0.5}
                >
                  Số sao đánh giá
                </Typography>
                <Rating name="half-rating-read" defaultValue={2.5} precision={0.5} readOnly />
              </Grid>
            </Grid>
          </Box>
          <Box mt={3}>
            <Typography
              fontSize="17px"
              color={theme.palette.primary.light}
              mb={0.5}
            >
              Hình ảnh game
            </Typography>
            <TextFieldCustom
              fullWidth
              name="imageUrl"
              placeholder="Nhập mô tả game"
              control={control}
              isRequired
              onBlur={handleOnBlur}
            />
          </Box>
          <Box mt={3}>
            {imageLink && (
              <Box
                sx={{
                  position: 'relative',
                }}
              >
                <img width="100%" src={imageLink} alt="Not Found Url" />
                <IconButton
                  onClick={handleClearImg}
                  sx={{
                    position: 'absolute',
                    top: '5px',
                    right: '5px',
                  }}
                >
                  <HighlightOffIcon sx={{ color: theme.palette.primary.light }} />
                </IconButton>
              </Box>
            )}
          </Box>
          <Box mt={3} display='flex'>
            <ButtonMore contentButton='Save' type='submit'/>
            <Box ml={2} sx={{ '& .MuiButtonBase-root': { backgroundColor: theme.palette.primary.red } }}>
              <ButtonMore contentButton='Clear' handleOnClick={handleOnClearInput}/>
            </Box>
          </Box>
        </Box>
      </Box>
    </form>
  )
}
