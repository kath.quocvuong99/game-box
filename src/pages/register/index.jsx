import { Button, TextField } from '@mui/material'
import React from 'react'
import { useForm } from 'react-hook-form'

export default function RegisterPage() {
  const name = 'firstName'
  const { register, handleSubmit, } = useForm()
  const handleOnBlur = (event) => {
    const { value } = event.target
    console.log(value)
  }
  const handleSub = (data) => {
    console.log(data)
  }
  return (
    <form onSubmit={handleSubmit(handleSub)}>
      <TextField {...register(name)} onBlur={handleOnBlur}/>
      <Button type='submit'>
        Submit
      </Button>
    </form>
  )
}
