import { Box, useTheme } from '@mui/material'
import React from 'react'
import Title from '../title'
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import DownloadIcon from '@mui/icons-material/Download';
import StarIcon from '@mui/icons-material/Star';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { useRef } from 'react';
import { useState } from 'react';
import Slider from 'react-slick';
import { useLayoutEffect } from 'react';
import { useEffect } from 'react';
import { getFuturedGame } from '../../services/futured-game';

export default function FeaturedGame() {
  const theme = useTheme()
  const { game } = useSelector(state => state.futuredgame)
  const slideFeatureGame = useRef(null)
  const dispathch = useDispatch()

  const FirstContent = styled('div')({
    fontSize: '15px',
    fontWeight: '700',
    color: theme.palette.primary.light,
  })
  const SecondContent = styled('div')({
    fontSize: '15px',
    color: theme.palette.primary.main,
  })
  const SlickButtonFixLeft = ({ currentSlide, slideCount, children, ...props }) => (
    <KeyboardArrowLeftIcon {...props} sx={{ color: theme.palette.primary.main }} />
  )
  const SlickButtonFixRight = ({ currentSlide, slideCount, children, ...props }) => (
    <KeyboardArrowRightIcon {...props} sx={{ color: theme.palette.primary.main }} />
  )
  useEffect(() => {
    getFuturedGame(
      (res) => {
				const { data } = res
				dispathch({
					type: 'SET_FUTEREDGAME',
					payload: {
						data
					}
				})
			},
			(err) => {
				console.log(err)
			}
    )
  }, [dispathch])

  const [positionLeft, setPositionLeft] = useState(0)
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: <SlickButtonFixRight/>,
    prevArrow: <SlickButtonFixLeft/>,
  }

  useLayoutEffect(() => {
    function cb() {
      setPositionLeft(slideFeatureGame?.current.offsetWidth - 17);
    }
    window.addEventListener('resize', cb);
    cb();
    return () => {
      window.removeEventListener('resize', cb);
    };
  }, [])

  return (
    <Box
      sx={{
        padding: '30px',
        backgroundColor: theme.palette.primary.darker,
        borderRadius: '23px',
        width: '58.6%',
        height: '100%'
      }}
    >
      <Box
        sx={{
          marginBottom: '30px',
        }}
      >
        <Title
          titleLeft='Featured'
          titleRight='Games'
        />
      </Box>
      <Box
        ref={slideFeatureGame}
        sx={{
          '& .slick-prev': {
            top: '0',
            left: positionLeft,
            zIndex: 1,
            '&:hover': {
              color: theme.palette.primary.lighter,
            }
          },
          '& .slick-next': {
            top: '0',
            zIndex: 1,
            '&:hover': {
              color: theme.palette.primary.lighter,
            }
          }
        }}
      >
        <Slider
          {...settings}
        >
          {
            game.map((item) => {
              return (
                <Box
                  key={item.id}
                >
                  <Box
                    sx={{
                      '& img': {
                        borderRadius: '23px',
                        width: '92%',
                        height: '100%',
                      },
                      width: '100%',
                      height: '47.1841704718417vh',
                    }}
                  >
                    <img src={item.imageUrl} alt={item.title} />
                  </Box>
                  <Box
                    sx={{
                      marginTop: '20px',
                      paddingRight: '8%'
                    }}
                  >
                    <Box
                      sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}
                    >
                      <FirstContent>{item.title}</FirstContent>
                      <Box
                        sx={{
                          display: 'flex',
                          alignItems: 'center',
                          fontSize: '15px',
                          color: theme.palette.primary.light,
                          '& svg': {
                            color: theme.palette.primary.yellow,
                            width: '16px',
                            height: '16px',
                          }
                        }}
                      >
                        <StarIcon />
                        &nbsp;{item.rate}
                      </Box>
                    </Box>
                    <Box
                      sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        marginTop: '7px',
                      }}
                    >
                      <SecondContent>{'249K Downloads'}</SecondContent>
                      <Box
                        sx={{
                          display: 'flex',
                          alignItems: 'center',
                          fontSize: '15px',
                          color: theme.palette.primary.light,
                          '& svg': {
                            color: theme.palette.primary.lighter,
                            width: '16px',
                            height: '16px',
                          }
                        }}
                      >
                        <DownloadIcon />
                        &nbsp;{item.download}
                      </Box>
                    </Box>
                  </Box>
                </Box>
              )
            })
          }
        </Slider>
      </Box>
    </Box>
  )
}
