import { Box, Button, Typography, useTheme } from '@mui/material'
import React from 'react'
import { useSelector } from 'react-redux'
import Title from '../title'
import CheckIcon from '@mui/icons-material/Check';

export default function TopStreamer() {
  const theme = useTheme()
  const { livestreams } = useSelector(state => state.mostpopularrightnow)

  return (
    <Box
      sx={{
        width: '27%',
        padding: '30px',
        backgroundColor: theme.palette.primary.darker,
        borderRadius: '23px',
        overflow: 'auto',
        maxHeight: '474px',
        '::-webkit-scrollbar': {
          width: '1px'
        }
      }}
    >
      <Box
        sx={{
          marginBottom: '30px',
        }}
      >
        <Title
          titleLeft='Top'
          titleRight='Streamers'
        />
      </Box>
      {
        livestreams.map((item, index) => {
          return (
            <Box
              key={item.id}
              sx={{
                width: '100%',
                borderBottom: `1px solid ${theme.palette.primary.dark}`,
                marginBottom: '25px',
                paddingBottom: '25px',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  '& img': {
                    width: '100%',
                    maxWidth: '46px',
                    borderRadius: '50%',
                    marginRight: '15px',
                  }
                }}
              >
                <Typography
                  sx={{
                    fontSize: '15px',
                    color: theme.palette.primary.light,
                    fontWeight: '700',
                    marginRight: '10px'
                  }}
                >
                  #{index + 1}
                </Typography>
                <img src={item.imageProfile} alt={item.streamName} />
                <Box
                  sx={{
                    backgroundColor: theme.palette.primary.lighter,
                    borderRadius: '50%',
                    padding: '1px 4px',
                    marginRight: '3px',
                    '& svg': {
                      color: theme.palette.primary.light,
                      width: '12px',
                      height: '12px',
                    }
                  }}
                >
                  <CheckIcon />
                </Box>
                <Typography
                  sx={{
                    color: theme.palette.primary.lighter,
                    marginLeft: '3px'
                  }}
                >
                  {item.streamName}
                </Typography>
              </Box>
              <Box
                sx={{
                  textAlign: 'end'
                }}
              >
                <Button
                  sx={{
                    padding: '6px 20px',
                    color: theme.palette.primary.light,
                    backgroundColor: theme.palette.primary.lighter,
                    textTransform: 'capitalize',
                    borderRadius: '25px',
                    letterSpacing: '0.5px',
                    ':hover': {
                      backgroundColor: theme.palette.primary.light,
                      color: theme.palette.primary.lighter,
                    },
                  }}
                >
                  Follow
                </Button>
              </Box>
            </Box>
          )
        })
      }
    </Box>
  )
}
