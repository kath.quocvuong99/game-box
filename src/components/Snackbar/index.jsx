import { Box, Snackbar } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { SNACKBAR_ACTIONS } from '../../actions';

export default function SnackbarCustom() {
  const { open, message } = useSelector((state) => state.snackbar);
  const dispatch = useDispatch();
  const handleClose = () => dispatch({ type: SNACKBAR_ACTIONS.CLOSE });

  return (
    <Box
      sx={{
        '& .MuiSnackbar-root': {
          top: '130px !important',
        },
      }}
    >
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={open}
        message={message}
        key={'top right'}
        onClose={handleClose}
        autoHideDuration={3000}
      />
    </Box>
  )
}
