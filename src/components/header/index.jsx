import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import logo from '../../assets/images/logo.png'
import logo_profile from '../../assets/images/profile-header.jpg'
import SearchIcon from '@mui/icons-material/Search';
import { Box } from '@mui/system';
import { Button, InputAdornment, TextField, useTheme } from '@mui/material';
import { useLocation, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { path } from '../../routers/path';
import { useState } from 'react';
import { LOGIN_ACTIONS } from '../../actions';

export default function Header() {
  const [navbarheader, setNavBarHeader] = useState(false)
  const { isAuthentication } = useSelector(state => state.authentication)
  const dispathch = useDispatch()
  const location = useLocation()
  const navigate = useNavigate()
  const { navHeader } = useSelector(state => state.navbar)
  const theme = useTheme()
  useEffect(() => {
    const { pathname } = location
    dispathch({
      type: 'UPDATE_LOCATION',
      payload: {
        pathname,
      },
    })
    window.addEventListener('scroll', handleScrll)
    return () => {
      window.removeEventListener('scroll', handleScrll)
    }
  }, [location, dispathch])
  const handleOnClickNav = (nav) => () => {
    if (nav.navName === 'Login') {
      dispathch({ type: LOGIN_ACTIONS.OPEN })
    } else {
      navigate(nav.path)
    }
  }

  const handleOnClickLogo = () => {
    navigate(path.home)
  }

  const handleScrll = () => {
    if (window.scrollY >= 70) {
      setNavBarHeader(true)
    } else {
      setNavBarHeader(false)
    }
  }

  return (
    <Box
      pb={navbarheader ? 2 : 0}
      sx={{
        marginBottom: '30px',
        position: navbarheader ? 'fixed' : 'unset',
        backgroundColor: navbarheader ? theme.palette.primary.darker : 'transparent',
        width: '100%',
        top: '0',
        left: '0',
        right: '0',
        zIndex: '1'
      }}
    >
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          padding: navbarheader ? '15px 7.686676427525622vw 0px 7.686676427525622vw' : '0'
        }}
      >
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            flex: '1',
          }}
        >
          <Box
            sx={{
              '& img': {
                paddingRight: '30px',
                cursor: 'pointer'
              },
              marginRight: '60px',
              borderRight: '1px solid #27292a'
            }}
          >
            <img src={logo} alt="Cyborg logo" onClick={handleOnClickLogo} />
          </Box>
          <Box>
            <TextField
              placeholder='Type Something'
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon
                      sx={{ color: 'white', zIndex: 1, }}
                    />
                  </InputAdornment>
                ),
              }}
              sx={{
                '& .MuiInputBase-root': {
                  borderRadius: '23px',
                  backgroundColor: theme.palette.primary.dark,
                  fontSize: '15px',
                  height: '46px'
                },
                '& .MuiInputBase-root:hover': {
                  border: '1px solid white'
                },
                '& .MuiInputBase-root:focus': {
                  border: 'none'
                },
                ' & .MuiInputBase-input': {
                  color: theme.palette.primary.main,
                },
              }}
            />
          </Box>
        </Box>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'flex-end',
            flex: '1',
          }}
        >
          {
            navHeader.map((item) => {
              if (item.id > 4) {
                if (isAuthentication) {
                  return (
                    <Button
                      sx={{
                        '& img': {
                          borderRadius: '50%',
                          width: '30px',
                          height: '30px',
                          ml: 1.5,
                        },
                        ':hover': {
                          backgroundColor: theme.palette.primary.lighter,
                          color: 'white'
                        },
                        borderRadius: '23px',
                        backgroundColor: theme.palette.primary.dark,
                        color: theme.palette.primary.main,
                        fontSize: '14px',
                        fontWeight: 'unset',
                        textTransform: 'unset',
                        p: '8px 10px 8px 20px',
                      }}
                    >
                      Logged
                      <img src={logo_profile} alt="Cyborg" />
                    </Button>
                  )
                } else {
                  return (
                    <Box
                      sx={{
                        padding: '13px 20px',
                        fontSize: '17px',
                        color: item.isActive ? theme.palette.primary.lighter : theme.palette.primary.main,
                        cursor: 'pointer',
                        ':hover': {
                          color: theme.palette.primary.lighter,
                        }
                      }}
                      onClick={handleOnClickNav(item)}
                      key={item.id}
                    >
                      {item.navName}
                    </Box>
                  )
                }
              }
              return (
                <Box
                  sx={{
                    padding: '13px 20px',
                    fontSize: '17px',
                    color: item.isActive ? theme.palette.primary.lighter : theme.palette.primary.main,
                    cursor: 'pointer',
                    ':hover': {
                      color: theme.palette.primary.lighter,
                    }
                  }}
                  onClick={handleOnClickNav(item)}
                  key={item.id}
                >
                  {item.navName}
                </Box>
              )
            })
          }
        </Box>

      </Box>
    </Box>
  )
}
