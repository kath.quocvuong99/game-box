import { Box, Button, Dialog, IconButton, Typography, useTheme } from '@mui/material';
import React from 'react'
import { useDispatch, useSelector } from "react-redux";
import { LOGIN_ACTIONS, SNACKBAR_ACTIONS } from '../../../actions';
import CloseIcon from '@mui/icons-material/Close';
import Logo from '../../../assets/images/logo.png';
import EmailIcon from '@mui/icons-material/Email';
import LockIcon from '@mui/icons-material/Lock';
import TextFieldCustom from '../../textfield-custum';
import styled from 'styled-components';
import iconGG from '../../../assets/logo/IconGoogle.svg'
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { path } from '../../../routers/path';


export default function AuthenticationDialog() {
  const theme = useTheme()
  const { isOpen } = useSelector((state) => state.login);
  const methods = useForm({
    mode: 'onSubmit',
    defaultValues: {
      email: '',
      password: '',
    }
  });
  const {
    handleSubmit,
    control,
    reset
  } = methods;
  const dispatch = useDispatch();
  const navigate = useNavigate()
  const SecondContent = styled('span')({
    color: theme.palette.primary.lighter,
    fontSize: '16px',
    fontWeight: '700',
    cursor: 'pointer'
  })
  const handleClose = () => {
    dispatch({ type: LOGIN_ACTIONS.CLOSE })
    clearInput()
  }
  const handleOnClickLogin = (data) => {
    console.log(data)
    navigate(path.game_create)
    clearInput()
    handleClose()
  }
  const clearInput = () => {
    reset();
  }
  const error = () => {
    dispatch({ type: SNACKBAR_ACTIONS.OPEN, message: 'Vui lòng kiểm tra dữ liệu' });
  }

  return (
    <Dialog
      onClose={handleClose}
      open={isOpen}
      sx={{
        '& .MuiPaper-root': {
          padding: '24px',
          minWidth: '520px',
          borderRadius: '36px',
          backgroundColor: theme.palette.primary.dark
        },
        overflowY: 'scroll',
        '::-webkit-scrollbar': {
          width: '1px'
        },
        "::-webkit-scrollbar-track": {
          background: theme.palette.primary.main,
        },
        "::-webkit-scrollbar-thumb ": {
          background: theme.palette.primary.lighter,
        },
        "::-webkit-scrollbar-thumb": {
          "&:hover": {
            background: theme.palette.primary.dark,
          },
        },
      }}
    >
      <form onSubmit={handleSubmit(handleOnClickLogin, error)}>
        <Box textAlign="end">
          <IconButton onClick={handleClose}>
            <CloseIcon sx={{ color: theme.palette.primary.light, }} />
          </IconButton>
        </Box>
        <Box textAlign="center">
          <img src={Logo} alt='Cyborg Logo' />
        </Box>
        <Typography
          fontSize="30px"
          textAlign="center"
          mt={1}
          sx={{
            '& span': {
              fontStyle: 'italic',
              color: theme.palette.primary.lighter
            },
            color: theme.palette.primary.light
          }}
        >
          Chào mừng bạn đến với <span>Cyborg</span>
        </Typography>
        <Box
          px={6}
          sx={{
            '& .MuiInputBase-root': {
              '& input': {
                color: theme.palette.primary.light
              },
              border: `solid 1px ${theme.palette.primary.main}`
            },
            '& svg': {
              color: theme.palette.primary.light
            }
          }}
        >
          <Box mt={1}>
            <TextFieldCustom
              name="email"
              placeholder="Email"
              type="text"
              fullWidth
              isStartIcon
              iconStart={<EmailIcon />}
              control={control}
              isRequired
            />
          </Box>
          <Box
            mt={2}
          >
            <TextFieldCustom
              name="password"
              placeholder="Password"
              type="password"
              fullWidth
              isStartIcon
              iconStart={<LockIcon />}
              control={control}
              isRequired
            />
          </Box>
          <Box
            sx={{
              cursor: 'pointer',
              display: 'flex',
              fontSize: '14px',
              marginTop: '8px',
              fontWeight: '700',
              justifyContent: 'end',
              marginBottom: '20px',
              color: theme.palette.primary.lighter,
            }}
          >
            Quên mật khẩu ?
          </Box>
          <Button
            type="submit"
            sx={{
              width: '100%',
              textTransform: 'capitalize',
              fontSize: '14px',
              color: theme.palette.primary.light,
              backgroundColor: theme.palette.primary.lighterminus,
              border: 'none',
              padding: '8px 30px',
              borderRadius: '23px',
              textAlign: 'center',
              fontWeight: '700',
              cursor: 'pointer',
              marginBottom: '20px',
              ':hover': {
                backgroundColor: theme.palette.primary.lighter,
              }
            }}
          >
            Đăng nhập
          </Button>
          <Button
            sx={{
              width: '100%',
              textTransform: 'capitalize',
              fontSize: '14px',
              color: theme.palette.primary.light,
              backgroundColor: 'transparent',
              border: `solid 1px ${theme.palette.primary.light}`,
              padding: '8px 30px',
              borderRadius: '23px',
              textAlign: 'center',
              fontWeight: '700',
              cursor: 'pointer',
              marginBottom: '20px',
              display: 'flex',
              alignItems: 'center',
              ' & img': {
                marginRight: '8px',
              }
            }}
          >
            <img src={iconGG} alt="google" />
            Tiếp tục với Gmail
          </Button>
          <Box
            sx={{
              color: theme.palette.primary.light,
              fontSize: '16px',
              margin: '18px 0px 18px 0px',
              textAlign: 'center',
              lineHeight: '20px',
            }}
          >
            Bằng cách tiếp tục, bạn đồng ý với các<br />
            <SecondContent>Điều khoản dịch vụ</SecondContent>&nbsp;của chúng tôi
          </Box>
          <Box
            sx={{
              color: theme.palette.primary.light,
              fontSize: '16px',
              margin: '18px 0px 18px 0px',
              textAlign: 'center',
              lineHeight: '20px',
            }}
          >
            Bạn chưa tham gia Cyborg?&nbsp;
            <SecondContent>Đăng ký</SecondContent>
          </Box>
          <Box
            sx={{
              color: theme.palette.primary.light,
              fontSize: '16px',
              margin: '18px 0px 18px 0px',
              textAlign: 'center',
              lineHeight: '20px',
            }}
          >
            Bạn là doanh nghiệp? Hãy bắt đầu&nbsp;
            <SecondContent>tại đây!</SecondContent>
          </Box>
        </Box>
      </form>
    </Dialog>
  )
}
