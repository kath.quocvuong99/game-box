export const typography = {
  subtitle1: {
    fontSize: '12px',
    fontWeight: 600,
    lineHeight: '24px',
  },
}
